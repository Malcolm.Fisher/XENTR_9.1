# Xenopus tropicalis 9.1 genome assembly files

This project houses gene models (GFF) and other metadata files for the X. laevis 9.2 genome assembly.

## File List

XENTR_9.1_Xenbase.gff - Gene models for X. tropicalis 9.1 genome assembly.

### Branches

Master - This branch holds the stable releases of the GFF file.

Name_metadata - This branch holds incremental changes to model names, dbxrefs, etc.

Structural - This branch holds incremental changes to gene model structures, including: adding, removing, and altering exons and transcripts, as well as adding/removing gene models.

### External Files

[Xenopus tropicalis 9.1 genome FASTA file](ftp://ftp.xenbase.org/pub/Genomics/JGI/Xentr9.1/XT9_1.fa.gz)



